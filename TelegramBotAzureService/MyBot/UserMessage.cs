﻿using MyBot.States;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace MyBot
{
    public class UserMessage
    {
        public Telegram.Bot.Types.Message  Msg { get; set; }
        public State State { get; set; }

        public UserMessage(Telegram.Bot.Types.Message msg)
        {
            State = new NeutralState();
            Msg = msg;
        }


        internal string FindOut(BotEvents botEvent, out IReplyMarkup replyMarkup)
        {
            return State.HandleEvent(this, botEvent, out replyMarkup);
        }
    }
}
