﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace MyBot.States
{
    public abstract class State
    {
        protected abstract string ChangeState(UserMessage message, BotEvents botEvent, out IReplyMarkup replyMarkup);

        internal string HandleEvent(UserMessage message, BotEvents botEvent, out IReplyMarkup replyMarkup)
        {
            return ChangeState(message, botEvent, out replyMarkup);
        }
    }
}



