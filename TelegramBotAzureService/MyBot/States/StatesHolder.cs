﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyBot.States
{
    public class StatesHolderSingleton
    {
        private Dictionary<string, State> states;
        #region Singleton pattern
        private static readonly Lazy<StatesHolderSingleton> instance = new Lazy<StatesHolderSingleton>(()=> new StatesHolderSingleton());
        public static StatesHolderSingleton Instance
        {
            get
            {
                return instance.Value;
            }
        }
        private StatesHolderSingleton()
        {
            states = new Dictionary<string, State>();
            states.Add("neutral", new NeutralState());
            states.Add("startRequested", new StartRequestedState());
            states.Add("started", new GameStartedState());
            states.Add("rejected", new GameRejectedState());
        }
        #endregion

       public State this[string name]
        {
            get
            {
                return states[name];
            }
        }        
    }
}
