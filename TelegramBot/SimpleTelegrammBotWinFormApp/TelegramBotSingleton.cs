﻿using LoggersLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

using Telegram.Bot;
using Telegram.Bot.Args;
using Telegram.Bot.Types.Enums;


namespace SimpleTelegrammBotWinFormApp
{
    public class TelegramBotSingleton
    {
        private static readonly Lazy<TelegramBotClient> instance;
        public static Telegram.Bot.Types.User Me { get; set; }
        public static string BotName { get; set; }

        static TelegramBotSingleton()
        {
            //SimpleTellegramTest03_bot
            instance = new Lazy<TelegramBotClient>(() => new TelegramBotClient("457256850:AAFlXJqWVebYQko5ZP2orbq_KwutPx6lEqA"));
           
        }

        public static TelegramBotClient Bot
        {
            get
            {
                return instance.Value;
            }
        }

        public static void Init()
        {
            Bot.OnMessage += BotOnMessageReceived; //пришло сообщение от бота
            Bot.OnCallbackQuery += Bot_OnButton; //была нажата зарегистрированная кнопка
            Bot.SetWebhookAsync(); //запуск работы бота через обертку над WebAPI
            Me = Bot.GetMeAsync().Result; //принудительный первый опрос бота
            //знакомство - это необязательная часть
            BotName = Me.Username;
            Bot.StartReceiving(); //запущен процесс постоянного асинхронного прослушивания
            LoggerLazyLoadSingleton.Instance.Use_D = true;
            LoggerLazyLoadSingleton.Instance.WriteProtocol("bot inited", "TelegramBotSingleton.Init", "");

        }

        private static void Bot_OnButton(object sender, CallbackQueryEventArgs e)
        {
            //сообщение от пользователя
            Telegram.Bot.Types.Message message = e.CallbackQuery.Message;
            string callbackQueryData = e.CallbackQuery.Data;
            LoggerLazyLoadSingleton.Instance.WriteProtocol($"button pushed: {callbackQueryData}", "TelegramBotSingleton.Bot_OnButton", "");
            StateMashine.DefineState(message, callbackQueryData);

        }

        private static void BotOnMessageReceived(object sender, MessageEventArgs e)
        {             
            //сообщение от пользователя
            Telegram.Bot.Types.Message msg = e.Message;
            //реагирую только на тексты
            //не на видео, картинки, кнопку купить и т.д.
            if (msg == null || msg.Type != MessageType.TextMessage)
                return;
            LoggerLazyLoadSingleton.Instance.WriteProtocol($"message received: {msg.Text}", "TelegramBotSingleton.BotOnMessageReceived", "");
            StateMashine.DefineState(e.Message);
        }

        
       
    }
}
