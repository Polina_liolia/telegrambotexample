﻿using LoggersLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Telegram.Bot.Types.ReplyMarkups;

namespace SimpleTelegrammBotWinFormApp.States
{
    public class StartRequestedState : State
    {
        protected override string ChangeState(UserMessage message, BotEvents botEvent, out IReplyMarkup replyMarkup)
        {
            LoggerLazyLoadSingleton.Instance.WriteProtocol($"start requested state", "StartRequestedState.ChangeState", "");
            string answer = "";
            replyMarkup = null;
            switch(botEvent)
            {
                case BotEvents.StartAccepted:
                    answer = "Поехали! Сделайте свой выбор: /stone, /scissors, /paper";
                    LoggerLazyLoadSingleton.Instance.WriteProtocol($"game started", "StartRequestedState.ChangeState", "");
                    message.State = StatesHolderSingleton.Instance["started"];
                    break;
                case BotEvents.StartRejected:
                    answer = "Поиграем в другой раз...";
                    LoggerLazyLoadSingleton.Instance.WriteProtocol($"game rejected", "StartRequestedState.ChangeState", "");
                    message.State = StatesHolderSingleton.Instance["rejected"];
                    break;
                default:
                    LoggerLazyLoadSingleton.Instance.WriteProtocol($"waiting for user choice", "StartRequestedState.ChangeState", "");
                    answer = "Вы еще не приняли решение, играть или нет. Нажмите на соответствующую кнопку.";
                    break;
            }
            return answer;
        }
    }
}
